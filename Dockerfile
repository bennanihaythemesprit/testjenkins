# Utiliser l'image OpenJDK 17 comme image de base
FROM khipu/openjdk17-alpine AS base

# Définir le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copier le fichier JAR dans le conteneur
COPY ./target/My-Demo-0.0.1-SNAPSHOT.jar .

# Exposer le port sur lequel votre application s'exécutera
EXPOSE 8080

# Commande pour exécuter l'application
ENTRYPOINT ["java", "-jar", "My-Demo-0.0.1-SNAPSHOT.jar"]

