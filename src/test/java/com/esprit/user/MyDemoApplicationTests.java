package com.esprit.user;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.esprit.user.entities.User;
import com.esprit.user.imp.UserService;

@SpringBootTest
class MyDemoApplicationTests {

	@Test
	public void testAddUser() {
		UserService userService= new UserService();
		User user = new User();
		user.setName("Haythem");
		user.setFirstName("Bennani");
		userService.Add_User(user);
		assertEquals(1,userService.find_User().size());
	}
	
	@Test
	public void testGetAllUsers() {
		
		UserService userService= new UserService();
		
		User user1 = new User();
        user1.setName("Doe");
        user1.setFirstName("John");

        User user2 = new User();
        user2.setName("Smith");
        user2.setFirstName("Alice");

        userService.Add_User(user1);
        userService.Add_User(user2);

        List<User> userList = userService.find_User();

        assertEquals(2, userList.size());
        assertEquals("Doe", userList.get(0).getName());
        assertEquals("John", userList.get(0).getFirstName());
        assertEquals("Smith", userList.get(1).getName());
        assertEquals("Alice", userList.get(1).getFirstName());
    }
	
	@Test
    public void testFindUserByIdNotFound() {
        // Test de la recherche d'un utilisateur par ID non trouvé
		UserService userService= new UserService();
        User foundUser = userService.findUserById(4);
        assertNull(foundUser);
    }
}
