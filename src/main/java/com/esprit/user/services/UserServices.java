package com.esprit.user.services;

import java.util.List;

import com.esprit.user.entities.User;

public interface UserServices {

	void Add_User(User user);
	List<User>find_User();
	User findUserById(long id);
}
