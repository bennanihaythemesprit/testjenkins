package com.esprit.user.imp;

import java.util.ArrayList;
import java.util.List;

import com.esprit.user.entities.User;
import com.esprit.user.services.UserServices;

public class UserService implements UserServices{

	private List<User> userList;
	public UserService() {
		this.userList=new ArrayList<>();
	}
	@Override
	public void Add_User(User user) {
				 
	userList.add(user);
	}

	@Override
	public List<User> find_User() {
		// TODO Auto-generated method stub
	
		return userList;
	}
	
    // Méthode pour rechercher un utilisateur par ID
	@Override
    public User findUserById(long id) {
        for (User user : userList) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

}
